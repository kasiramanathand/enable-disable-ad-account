Import-Module ActiveDirectory

$userIDs = Import-Csv -Path .\Desktop\Book1.csv

Write-Host "Kindly select an action to perform `n"
Write-Host "1. Enable User Account"
Write-Host "2. Disable User Account `n"

$Action = Read-Host "Enter Your Choice"

if($Action -eq "1"){
    foreach($userID in $userIDs){
        $Name = $userID.UserName
        $Description = $userID.Description

        if((Get-ADUser $Name –Properties Enabled).Enabled -eq "True") {
            Write-Host "$Name has already been enabled"
        }
        else {
            Get-ADUser -Identity $Name | Enable-ADAccount
            Set-ADUser $Name -Description $Description

            if ((Get-ADUser $Name –Properties Enabled).Enabled -eq "True") {
                Write-Host "$Name has been enabled successfully"
            }
            else {
                Write-Host "$Name is not enabled. Please contact the administrator for further processing"
            }
        }
    }
    Write-Host " "
    Write-Host "Activity has been completed successfully"
}
elseif($Action -eq "2"){
    foreach($userID in $userIDs){
        $Name = $userID.UserName
        $Description = $userID.Description

        if((Get-ADUser $Name –Properties Enabled).Enabled -eq "False") {
            Write-Host "$Name has already been disabled"
        }
        else {
            Get-ADUser -Identity $Name | Disable-ADAccount
            Set-ADUser $Name -Description $Description

            if ((Get-ADUser $Name –Properties Enabled).Enabled -eq "False") {
                Write-Host "$Name has been disabled successfully"
            }
            else {
                Write-Host "$Name is not disabled. Please contact the administrator for further processing"
            }
        }
    }
    Write-Host " "
    Write-Host "Activity has been completed successfully"
}
else{
    Write-Host "Invalid Choice. Kindly exit and re-run the script `n"
    Write-Host "The Valid Choices are either 1 or 2"
}
Write-Host "************** End of the Script **************"
Read-Host -Prompt "Press Enter to Exit"

